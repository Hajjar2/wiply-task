// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBATfyKqgPLVBo1H4Lcp2SwGbNuNaaU1XI",
    authDomain: "wiply-task.firebaseapp.com",
    projectId: "wiply-task",
    storageBucket: "wiply-task.appspot.com",
    messagingSenderId: "354021547988",
    appId: "1:354021547988:web:9fa810762eba0935760041"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
