import { FirestoreService } from './../firestore.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

  public squaresArray;
  selectedBgColor;

  constructor(private firestoreService: FirestoreService) { }

  ngOnInit(): void {
    this.firestoreService.getSquaresList().subscribe(list=> this.squaresArray = list);
  }

  onSquareColorChange(event) {
    this.firestoreService.onSave(event);
  }

}
