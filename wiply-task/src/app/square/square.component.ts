import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent implements OnInit {
  @Input() squareId;
  @Input() squareBgColor = "#0000";
  @Output() square = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  getRandomColor() {
    var length = 6;
    var chars = '0123456789ABCDEF';
    var hex = '#';
    while(length--) hex += chars[(Math.random() * 16) | 0];
    this.squareBgColor = hex;
    this.square.emit({id: this.squareId, color: this.squareBgColor});
  }

}
