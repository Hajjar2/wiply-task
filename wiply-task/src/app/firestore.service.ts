import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(private angularFirestore: AngularFirestore) { }

  onSave(square){
    this.angularFirestore.collection('squares').doc(square.id.toString()).update(square);
  }

  getSquaresList(){
    return this.angularFirestore.collection('squares').valueChanges({ idField: 'id' });
  }
}
